import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import java.awt.Panel;
import javax.swing.JComboBox;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.DropMode;
import java.awt.Label;
import javax.swing.JButton;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import java.awt.Toolkit;
import javax.swing.JToggleButton;
import javax.swing.JTable;
import javax.swing.JDesktopPane;

/***
 * 
 * @author Andrea Mari�osa
 * 
 * @since 20/01/2018
 *
 */

public class Proyecto extends JFrame {

	private JPanel contentPane;
	private JTextField txtCantidad;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Proyecto frame = new Proyecto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Proyecto() {
		setFont(new Font("Open Sans", Font.PLAIN, 12));
		setIconImage(Toolkit.getDefaultToolkit().getImage(Proyecto.class.getResource("/Image/asdfds.ico")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 449, 302);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setForeground(Color.BLACK);
		menuBar.setBackground(Color.PINK);
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setForeground(new Color(128, 0, 128));
		menuBar.add(mnFile);
		
		JMenuItem mntmSaveAs = new JMenuItem("Save as");
		mnFile.add(mntmSaveAs);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mnFile.add(mntmNew);
		
		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setForeground(new Color(128, 0, 128));
		menuBar.add(mnEdit);
		
		JMenuItem mntmNewProyect = new JMenuItem("New Proyect");
		mnEdit.add(mntmNewProyect);
		
		JMenuItem mntmCopyProyect = new JMenuItem("Copy Proyect");
		mnEdit.add(mntmCopyProyect);
		
		JMenuItem mntmImportProyect = new JMenuItem("Import Proyect");
		mnEdit.add(mntmImportProyect);
		
		Panel panel = new Panel();
		mnEdit.add(panel);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(245, 245, 245));
		contentPane.setBackground(new Color(216, 191, 216));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Button button = new Button("Comprar");
		button.setBackground(Color.DARK_GRAY);
		button.setBounds(10, 11, 70, 22);
		contentPane.add(button);
		
		Button button_1 = new Button("Vender");
		button_1.setBackground(Color.DARK_GRAY);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button_1.setBounds(10, 39, 70, 22);
		contentPane.add(button_1);
		
		Button button_2 = new Button("Salir");
		button_2.setBackground(Color.DARK_GRAY);
		button_2.setBounds(10, 67, 70, 22);
		contentPane.add(button_2);
		
		JLabel lblDetalles = new JLabel("Detalles:");
		lblDetalles.setFont(new Font("Tw Cen MT", Font.PLAIN, 15));
		lblDetalles.setBounds(10, 148, 86, 22);
		contentPane.add(lblDetalles);
		
		JTextPane txtpnFundaIphone = new JTextPane();
		txtpnFundaIphone.setBackground(new Color(216, 191, 216));
		txtpnFundaIphone.setForeground(new Color(128, 0, 128));
		txtpnFundaIphone.setFont(new Font("Tw Cen MT", Font.PLAIN, 25));
		txtpnFundaIphone.setText("Funda iPhone");
		txtpnFundaIphone.setBounds(267, 0, 140, 28);
		contentPane.add(txtpnFundaIphone);
		
		txtCantidad = new JTextField();
		txtCantidad.setForeground(Color.BLACK);
		txtCantidad.setEditable(false);
		txtCantidad.setText("Cantidad:");
		txtCantidad.setBounds(86, 11, 61, 20);
		contentPane.add(txtCantidad);
		txtCantidad.setColumns(10);
		
		JSpinner spinner = new JSpinner();
		spinner.setBackground(Color.LIGHT_GRAY);
		spinner.setBounds(146, 11, 38, 20);
		contentPane.add(spinner);
		
		JTextPane txtpnCarcasaDePolicarbonato = new JTextPane();
		txtpnCarcasaDePolicarbonato.setForeground(Color.WHITE);
		txtpnCarcasaDePolicarbonato.setBackground(Color.PINK);
		txtpnCarcasaDePolicarbonato.setText("Carcasa de policarbonato resistente a los golpes que permite el acceso a todos los puertos del dispositivo.");
		txtpnCarcasaDePolicarbonato.setBounds(10, 166, 234, 64);
		contentPane.add(txtpnCarcasaDePolicarbonato);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setLabelFor(this);
		lblNewLabel.setIcon(new ImageIcon(Proyecto.class.getResource("/Image/mwo,210x210,iphone_8_snap-pad,210x230,f8f8f8.3u4..png")));
		lblNewLabel.setBounds(288, 35, 105, 180);
		contentPane.add(lblNewLabel);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(417, 0, 17, 245);
		contentPane.add(scrollBar);
		
		textField = new JTextField();
		textField.setText("Cantidad:");
		textField.setForeground(Color.BLACK);
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(86, 41, 61, 20);
		contentPane.add(textField);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBackground(Color.LIGHT_GRAY);
		spinner_1.setBounds(146, 41, 38, 20);
		contentPane.add(spinner_1);
		
		JSlider slider = new JSlider();
		slider.setBounds(288, 212, 105, 22);
		contentPane.add(slider);
	}
}
