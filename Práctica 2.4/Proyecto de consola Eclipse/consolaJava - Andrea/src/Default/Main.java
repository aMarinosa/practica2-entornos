package Default;

import java.util.Scanner;

public class Main {
static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		int opcion =0;
			
		while(opcion!=5){
			System.out.println("-----MENU-----\n"
					+ "[1] Sumar\n"
					+ "[2] Restar\n"
					+ "[3] Multiplicar\n"
					+ "[4] Dividir\n"
					+ "[5] Salir");
			opcion = in.nextInt();
			int n1=0, n2=0;
			switch (opcion){
			case 1:
				sumar(n1, n2);
				break;
			case 2:
				restar(n1, n2);
				break;
			case 3:
				multiplicar(n1, n2);
				break;
			case 4:
				dividir(n1, n2);
				break;
			case 5:
				System.exit(0);
			}
		}
	}

	private static float dividir(int n1, int n2) {
		System.out.println("Primer n�mero: ");
		n1 = in.nextInt();
		System.out.println("Segundo n�mero: ");
		n2 = in.nextInt();
		
		float div=(float)n1/n2;
		System.out.println(div);
		return div;
		
	}

	private static int multiplicar(int n1, int n2) {
		System.out.println("Primer n�mero: ");
		n1 = in.nextInt();
		System.out.println("Segundo n�mero: ");
		n2 = in.nextInt();
		int mult = n1*n2;
		System.out.println(mult);
		return mult;
		
	}

	private static int restar(int n1, int n2) {
		System.out.println("Primer n�mero: ");
		n1 = in.nextInt();
		System.out.println("Segundo n�mero: ");
		n2 = in.nextInt();
		int rest = n1-n2;
		System.out.println(rest);
		return rest;
		
	}

	private static int sumar(int n1, int n2) {
		
		System.out.println("Primer n�mero: ");
		n1 = in.nextInt();
		System.out.println("Segundo n�mero: ");
		n2 = in.nextInt();
		int sum= n1+n2;
		System.out.println(sum);
		return sum;
	}

}
