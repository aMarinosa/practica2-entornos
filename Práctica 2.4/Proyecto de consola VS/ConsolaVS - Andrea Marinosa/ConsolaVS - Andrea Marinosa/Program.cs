﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS___Andrea_Marinosa
{
    class Program
    {
        static void Main(string[] args)
        {
            string opcion1 = "0";
            int opcion2 = Convert.ToInt32(opcion1);

            while (opcion2 < 5 && opcion2 >= 0)
            {
                System.Console.WriteLine("-----MENU-----\n"
                        + "[1] Sumar\n"
                        + "[2] Restar\n"
                        + "[3] Multiplicar\n"
                        + "[4] Dividir\n"
                        + "[5] Salir");
                string opcion3 = Console.ReadLine();
                opcion2 = Convert.ToInt32(opcion3);
                
                int n1 = 0, n2 = 0;
                switch (opcion2)
                {
                    case 1:
                        sumar(n1, n2);
                        break;
                    case 2:
                        restar(n1, n2);
                        break;
                    case 3:
                        multiplicar(n1, n2);
                        break;
                    case 4:
                        dividir(n1, n2);
                        break;
                    case 5:
                        break;
                }
            }
            
        }

        private static float dividir(int n1, int n2)
        {
            System.Console.WriteLine("Primer número: ");
            string num1 = Console.ReadLine();
            n1 = Convert.ToInt32(num1); 
            System.Console.WriteLine("Segundo número: ");
            string num2 = Console.ReadLine();
            n2 = Convert.ToInt32(num2);

            float div = (float)n1 / n2;
            System.Console.WriteLine(div);
            return div;

        }

        private static int multiplicar(int n1, int n2)
        {
            System.Console.WriteLine("Primer número: ");
            string num1 = Console.ReadLine();
            n1 = Convert.ToInt32(num1);
            System.Console.WriteLine("Segundo número: ");
            string num2 = Console.ReadLine();
            n2 = Convert.ToInt32(num2);
            int mult = n1 * n2;
            System.Console.WriteLine(mult);
            return mult;

        }

        private static int restar(int n1, int n2)
        {
            System.Console.WriteLine("Primer número: ");
            string num1 = Console.ReadLine();
            n1 = Convert.ToInt32(num1);
            System.Console.WriteLine("Segundo número: ");
            string num2 = Console.ReadLine();
            n2 = Convert.ToInt32(num2);
            int rest = n1 - n2;
            System.Console.WriteLine(rest);
            return rest;

        }

        private static int sumar(int n1, int n2)
        {

            System.Console.WriteLine("Primer número: ");
            string num1 = Console.ReadLine();
            n1 = Convert.ToInt32(num1);
            System.Console.WriteLine("Segundo número: ");
            string num2 = Console.ReadLine();
            n2 = Convert.ToInt32(num2);
            int sum = n1 + n2;
            System.Console.WriteLine(sum);
            return sum;
        }
       
    }
    
}
