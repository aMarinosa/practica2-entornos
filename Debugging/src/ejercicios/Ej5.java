package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		/*
		 * Este ejercicio te muestra si un n�mero que has introducido por teclado es primo o no.
		 * 
		 * Inicializa un bucle, si la variable que ha sido le�da se divide entre la variable que ha 
		 * inicializado(en el bucle) y cumple la condici�n de tener m�s de 2 divisores, significar� que no es primo.
		 *  
		 */
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
