package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		/*
		 * El error est� en �cadenaLeida�; no llegamos a poder asignarle el valor porque 
		 * no est� limpio el buffer. Por esto, su valor siempre ser� cadenaLeida=��
		 * 
		 * Una posible soluci�n ser�a a�adir "input.next();" antes de pedir la "cadenaLeida",
		 * o en su defecto, escribir la cadena antes del int.
		 */
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
