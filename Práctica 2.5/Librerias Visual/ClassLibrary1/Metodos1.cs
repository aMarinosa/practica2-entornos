﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Metodos1
    {
        public static void numPar(int numero)
        {
            if (numero % 2 == 0)
            {
                Console.WriteLine("Es par.");
            }
            else
            {
                Console.WriteLine("Es impar.");
            }

        }
        public static int numPotencia(int numero)
        {
            int result, p = numero;
            result = numero * numero;
            p--;

            while (p != 1)
            {
                result *= numero;
                p--;
            }
            return result;
        }
        public static int numFactorial(int numero)
        {
            int factorial = 1;
            while (numero != 0)
            {
                factorial = factorial * numero;
                numero--;
            }
            if (numero == 0)
                return 1;
            else
                return numero * numFactorial(numero - 1);
        }

        public static int numPerfecto(int numero)
        {
            int sumas = 0;
            for (int i = 1; i < numero - 1; i++)
            {
                if (numero % i == 0)
                    sumas += i;
            }
            if (sumas == numero)
            {
               Console.WriteLine("El numero " + numero + " es un numero perfecto;");
                return numero;
            }
            else
            {
                Console.WriteLine("El numero " + numero + " no es un numero perfecto;");
                return numero;
            }
        }

        public static Boolean numPrimo(int numero)
        {
            int contador;
            contador = 2;
            Boolean primo = true;
            while ((primo) && (contador != numero))
            {
                if (numero % contador == 0)
                {
                    primo = false;
                }
                contador++;
            }
            return primo;

        }
    }
   
}
