﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaLibreriasaMarinosa
{
    class Program
    {
        static void Main(string[] args)
        {
            // Metodos 1 de la Libreria Visual


            Console.WriteLine("Num factorial: "+ClassLibrary1.Metodos1.numFactorial(2000));
            ClassLibrary1.Metodos1.numPar(8);
            Console.WriteLine(ClassLibrary1.Metodos1.numPerfecto(9));
            Console.WriteLine("Potencia de 6 "+ClassLibrary1.Metodos1.numPotencia(6));
            Console.WriteLine("Es primo? "+ClassLibrary1.Metodos1.numPrimo(13));

            // Metodos 2 de la Libreria Visual
            Console.WriteLine(ClassLibrary1.Metodos2.divisionNum(10, 2));
            ClassLibrary1.Metodos2.maxNum(100, 54);
            Console.WriteLine(ClassLibrary1.Metodos2.multiplicacionNum(3, 2));
            Console.WriteLine(ClassLibrary1.Metodos2.restaNum(10, 9));
            Console.WriteLine(ClassLibrary1.Metodos2.sumaNum(5, 6));

            Console.ReadKey();
        }
    }
}
