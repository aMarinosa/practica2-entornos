import librerias.Clase1;
import librerias.Clase2;

public class Main {

	public static void main(String[] args) {
		System.out.println(Clase1.numFactorial(1));
		System.out.println(Clase1.numPerfecto(5));
		System.out.println(Clase1.numPotencia(2));
		System.out.println(Clase1.numPrimo(7));
		Clase1.numPar(8);
		System.out.println(Clase2.divisionNum(10, 2));
		System.out.println(Clase2.maxNum(10, 20));
		System.out.println(Clase2.multiplicacionNum(6, 10));
		System.out.println(Clase2.restaNum(30, 20));
		System.out.println(Clase2.sumaNum(5, 7));
		

	}

}
