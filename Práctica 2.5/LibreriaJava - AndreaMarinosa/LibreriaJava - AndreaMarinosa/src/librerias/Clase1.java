package librerias;
public class Clase1 {

	public static void numPar(int numero) {
		if(numero%2==0){
			System.out.println("Es par.");
		}
		else{
			System.out.println("Es impar.");
		}
		
	}

	public static boolean numPrimo(int numero){
	  int contador = 2;
	  boolean primo=true;
	  while ((primo) && (contador!=numero)){
	    if (numero % contador == 0){
	    	primo = false;
	    }
	    contador++;
	  }
	  return primo;

	}
	
	public static int numFactorial(int numero){
		int factorial=1;
		while (numero!=0) {
			  factorial=factorial*numero;
			  numero--;
			}
		if (numero==0)
		    return 1;
		else
		    return numero * numFactorial(numero-1);
	}
	
	public static int numPerfecto(int numero){
        int sumas=0;
        for(int i = 1;i < numero - 1;i++){
                if(numero % i == 0)
                    sumas += i;
                }
        if(sumas == numero) {
            System.out.println("El numero "+numero+" es un numero perfecto;");
            return numero;
        } else {
            System.out.println("El numero "+numero+" no es un numero perfecto;");
            return numero;
        }
	}
	public static int numPotencia(int numero){
		int result, p=numero;
	        result = numero*numero;
	        p--;
	        
	        while(p!=1){
	            result *= numero;
	            p--;
	        }
	        return result;
	}
			    
}
